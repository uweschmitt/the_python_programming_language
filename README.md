# About

This is the repository for the Introduction to Python course.


## Development setup

1. Create a virtualenvironment named `venv` in the same folder as this README file.
2. Activate the environment.
3. Run `pip install -f setup/requirements.txt -r setup/requirements_dev.txt`
4. Run `pre-commit install`

This will enable all [pre-commit](https://pre-commit.com/) hooks which:

- run black and isort on the affected notebooks
- clear notebook output cells and remove meta data to reduce diffs in merge requests
- collapse solutino cells
- run a spell checker on the markdown and html content of notebooks

### Spellchecker

You can whitelist words by adding new lines to the `spellchecker_whitelist.txt` file.
