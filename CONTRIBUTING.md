
# Contributing

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.


# Report Bugs or Typos

Please use the issue tracker to report any issues with the content.

# Fix Bugs or Typos

Please follow the instructions from the [README](README.md) to setup a proper
development environment before you submit a pull request.

In case you include foreign content (e.g. graphics or copy/pasted text): please
make sure that the contents license is compatible with our [LICENSE](LICENSE)
and mark these contributions accordingly.
