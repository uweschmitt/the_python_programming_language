#!/usr/bin/env python

import json
import os
import sys

from collections import UserDict

from nbclient import NotebookClient

"""
1. executes cell with tag "init-cell"
2. clears output for all other cell
3. removes ids
4. collapses solution cells
"""


COLLAPSE_META = "jp-MarkdownHeadingCollapsed"


class DD(UserDict):
    __getattr__ = UserDict.__getitem__


def fix(dd):
    if isinstance(dd, dict):
        items = fix(list(dd.items()))
        return DD(items)
    if isinstance(dd, (tuple, list, set)):
        values = []
        for item in dd:
            values.append(fix(item))
        if isinstance(dd, tuple):
            return tuple(values)
        if isinstance(dd, set):
            return set(values)
        return values
    return dd


class DDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, DD):
            return dict(obj)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


for p in sys.argv[1:]:
    if not p.endswith(".ipynb"):
        continue

    with open(p) as fh:
        print(p)
        nb = fix(json.load(fh))

    nb.metadata = {}

    client = NotebookClient(
        nb, timeout=600, resources={"metadata": {"path": os.path.dirname(p)}}
    )
    with client.setup_kernel():
        print("COLLAPS SOLUTIONS AND OPTIONAL SECTIONS IN", p)
        for cell in nb["cells"]:
            cell["metadata"].pop(COLLAPSE_META, None)
        for i, cell in enumerate(nb["cells"]):
            cell.pop("id", None)
            if cell["cell_type"] == "code":
                if "init-cell" in cell["metadata"].get("tags", []):
                    cell.source = "".join(cell.source)
                    client.execute_cell(cell, i)
                    cell.source = [cell.source]
                    cell.execution_count = None
                    cell.metadata.execution.clear()
                else:
                    cell["outputs"].clear()
                cell["execution_count"] = None
                continue
            if any(
                'class="solutions"' in line.replace(" ", "") for line in cell["source"]
            ) or any(
                'class="optional"' in line.replace(" ", "") for line in cell["source"]
            ):
                if cell["metadata"].get(COLLAPSE_META) != "true":
                    print("COLLAPSE CELL", cell["source"])
                    cell["metadata"][COLLAPSE_META] = "true"
    print("WRITE", p)
    with open(p, "w") as fh:
        json.dump(nb, fh, indent=2, cls=DDEncoder)
