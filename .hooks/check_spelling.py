import io
import json
import os
import re
import subprocess
import sys

import mistune

HERE = os.path.dirname(os.path.abspath(__file__))

PERSONAL_DICT = os.path.realpath(os.path.join(HERE, "..", "spellchecker_whitelist.txt"))


ok = True
for p in sys.argv[1:]:
    if not p.endswith(".ipynb"):
        continue

    with open(p) as fh:
        nb = json.load(fh)

    texts = []

    for cell in nb["cells"]:
        if cell["cell_type"] != "markdown":
            continue
        texts.extend(cell["source"])
        texts.append("\n")

    md = "".join(texts)

    md = re.sub(r"\$\$[^$]+\$\$", "", md, re.MULTILINE)
    md = re.sub(r"```[^`]+```", "", md, re.MULTILINE)

    md = re.sub(r"\$.+\$", "", md)
    md = re.sub(r"`.+`", "", md)

    html = mistune.html(md)
    stdin = io.StringIO(html)
    stdout = io.StringIO()

    proc = subprocess.Popen(
        ["aspell", "--mode", "html", "-p", PERSONAL_DICT, "-l", "en_GB", "list"],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        text=True,
    )
    proc.stdin.write(html)
    proc.stdin.close()
    misspelled = set(proc.stdout.read().splitlines())
    if misspelled:
        print(p)
        for m in sorted(misspelled):
            print("   ", m)
        ok = False

sys.exit(0 if ok else 1)
